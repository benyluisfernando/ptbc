import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelSetupComponent } from './channel-setup.component';

describe('ChannelSetupComponent', () => {
  let component: ChannelSetupComponent;
  let fixture: ComponentFixture<ChannelSetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelSetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
