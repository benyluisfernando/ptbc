import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationSetupComponent } from './integration-setup.component';

describe('IntegrationSetupComponent', () => {
  let component: IntegrationSetupComponent;
  let fixture: ComponentFixture<IntegrationSetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntegrationSetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
