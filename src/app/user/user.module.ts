import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { RouterModule, Routes } from '@angular/router';
import { SessionComponent } from './session/session.component';
import { RoleComponent } from './role/role.component';
import { IntegrationSetupComponent } from './integration-setup/integration-setup.component';
import { CompanySetupComponent } from './company-setup/company-setup.component';
import { ChannelSetupComponent } from './channel-setup/channel-setup.component';
import { PendingReqComponent } from './pending-req/pending-req.component';
import { CompanyReqComponent } from './company-req/company-req.component';
import { ChannelReqComponent } from './channel-req/channel-req.component';
import { EditComponent } from './channel-setup/edit/edit.component';
import { AddComponent } from './channel-setup/add/add.component';
import { ViewComponent } from './channel-setup/view/view.component';
import { DeleteComponent } from './channel-setup/delete/delete.component';


const routes : Routes = [
  {
    path:'',component:MainComponent,
    children:[
      {
        path:'',
        redirectTo:'/user',
        pathMatch:'full'
      },
      {
        path:'home',component:MainComponent
      },
      {
        path:'role',component:RoleComponent
      },
      {
        path:'session',component:SessionComponent
      },
      {
        path:'channel-req',component:ChannelReqComponent
      },
      {
        path:'channel-setup',component:ChannelSetupComponent
      },
      {
        path:'company-req',component:CompanyReqComponent
      },
      {
        path:'company-setup',component:CompanySetupComponent
      },
      {
        path:'integration-setup',component:IntegrationSetupComponent
      },
      {
        path:'pending-req',component:PendingReqComponent
      },
      {
        path:'edit',component:EditComponent
      },
      {
        path:'view',component:ViewComponent
      },
      {
        path:'add',component:AddComponent
      },
      {
        path:'delete',component:DeleteComponent
      }
    ]
  }

]

@NgModule({
  declarations: [MainComponent, SessionComponent, RoleComponent, IntegrationSetupComponent, CompanySetupComponent, ChannelSetupComponent, PendingReqComponent, CompanyReqComponent, ChannelReqComponent, EditComponent, AddComponent, ViewComponent, DeleteComponent],
  imports: [
    CommonModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    RouterModule.forChild(routes)
  ]
})
export class UserModule { }
