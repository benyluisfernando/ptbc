testApp.controller("ajaxController", function ($scope, $http) {
  $scope.loadData = function () {
    $http.get("data.json").success(function (data) {
      $scope.products = data;
    });
  };

  $scope.loadDataPromise = function () {
    $http
      .get("PTBC - Unit Testing.postman_collection.json")
      .then(function (response) {
        console.log("Status: " + response.status);
        console.log("Type: " + response.headers("content-type"));
        console.log("Length: " + response.headers("content-length"));
        $scope.products = response.data;
      });
  };
});
