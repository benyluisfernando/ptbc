import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-company-setup',
  templateUrl: './company-setup.component.html',
  styleUrls: ['./company-setup.component.scss'],
})
export class CompanySetupComponent implements OnInit {
  title = 'app';
  results: any = {};

  // Inject HttpClient into your component or service.
  constructor() {}
  ngOnInit(): void {
    console.log(this.results);
  }
}
