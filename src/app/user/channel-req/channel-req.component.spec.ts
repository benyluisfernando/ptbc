import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelReqComponent } from './channel-req.component';

describe('ChannelReqComponent', () => {
  let component: ChannelReqComponent;
  let fixture: ComponentFixture<ChannelReqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelReqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelReqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
