import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

var myExtObject = (function() {

  return {
    func1: function() {
      alert('function 1 called');
    },
    func2: function() {
      alert('function 2 called');
    }
  }

})


var webGlObject = (function() { 
  return { 
    init: function() { 
      alert('webGlObject initialized');
    } 
  } 
})

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) {}

  
}
